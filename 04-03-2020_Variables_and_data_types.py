#!/usr/bin/env python
# coding: utf-8

# In[ ]:


get_ipython().set_next_input('/know what is variable');get_ipython().run_line_magic('pinfo', 'variable')

do's:
* s = 5
* _s = 6
* s420 = 7
dont's:
*7s  = 4
*?s = 5


# In[ ]:


data types:
    *intiger [5,-9,0,etc]
    *float[5.0,-6.5,7.987,etc]
    *boolean [true,false]


# In[2]:


print('hello world')


# In[4]:


s = 5
print (s)


# In[6]:


s = 5
s = 6
print(s)


# In[8]:


s = 4
_s = 5
s4 = 6
print(s,_s,s4)


# In[10]:


get_ipython().run_line_magic('pinfo', 's')
print(?s)


# In[23]:


int = 5
float = -0.003
# bool = True
print(bool)
# boolean = False
# print(bool)
print(int,float)


# In[ ]:


exe:
    how to make bool value 'false'?


# In[25]:


S = 5
s = 6
print(s,S)


# In[59]:


subash = 5
subash= 2
subash = 9
subash = 1
subash = 'subash'
print(subash)


# In[ ]:




